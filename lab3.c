#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main()
{
    int num1,num2,num3,large;
    printf("\n enter 1st num: ");
    scanf("%d",&num1);
    printf("\n enter 2nd num: ");
    scanf("%d",&num2);
    printf("\n enter 3rd num: ");
    scanf("%d",&num3);

    large = greater(num1,num2,num3);
    printf("\n largest num = %d",large);

    return 0;
}


int greater(int a, int b,int c)
{
    if(a>b && a>c)
        return a;
    else if (b>a && b>c)
        return b;
    else
        return c;
}
