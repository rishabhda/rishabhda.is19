#include <stdio.h>
#include <stdlib.h>

int main()
{
  int matrix[10][10];
  int tran[10][10];
  int i,j,a,b;

 printf("enter the rows and columns of the matrix: ");
 scanf("%d%d",&a,&b);
printf("enter the elements in the matrix: ");

 for(i=0;i<a;i++)
 {
     for(j=0;j<b;j++)
     {
         scanf("%d",&matrix[i][j]);
     }
 }

 for(i=0;i<a;i++)
 {
     for(j=0;j<b;j++)
     {
         printf("%d ",matrix[i][j]);
     }
     printf("\n");
 }

 printf("the transpose of matrix is:\n");

 for(i=0;i<b;i++)
 {
     for(j=0;j<a;j++)
     {
       tran[i][j]=matrix[j][i];    
     }
     
 }

 for(i=0;i<a;i++)
 {
     for(j=0;j<b;j++)
     {
         printf("%d ",tran[i][j]);
     }
     printf("\n");
 }
		return 0;
		}
