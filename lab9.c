#include <stdio.h>
int swap(int *p,int *q);
int main()
{
  int a,b,*p,*q;
  printf("enter 2 numbers:\n");
  scanf("%d%d",&a,&b);
  p=&a;q=&b;
  swap(p,q);
  return 0;
}

int swap(int *p,int *q)
{
 int t;
 printf("\nnumbers before swapping:\nnum1:%d\nnum2:%d",*p,*q);
 t=*p;*p=*q;*q=t;
  printf("\nnumbers after swapping:\nnum1:%d\nnum2:%d",*p,*q);
  return 1;
  }