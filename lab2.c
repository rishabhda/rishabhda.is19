#include <stdio.h>
#include <stdlib.h>
#include <math.h>
int mins(int hour,int minutes);
int main()
{
    int hr,min,inmin;
    printf("enter hour in 24-hr format: ");
    scanf("%d",&hr);
    printf("enter minutes: ");
    scanf("%d",&min);
    inmin = mins(hr,min);
    printf("time in minutes is %d",inmin);
    return 0;
}
int mins(int hour,int minutes)
{
    int res;
    res = (hour*60) + minutes;
    return res;

}