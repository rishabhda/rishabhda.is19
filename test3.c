#include <stdio.h>
#include <math.h>

int main()
{
int a,b,c;
float d,r1,r2;

printf ("the quadratic equation is ax^2+bx+c\n");
printf ("enter a,b,c\n");
scanf ("%d%d%d",&a,&b,&c);

d = ((pow(b,2)) - (4*a*c));
r1 = (-b + sqrt(d))/(2*a);
r2 = (-b - sqrt(d))/(2*a);

if (a!=0) {
if (d == 0) {
printf ("roots are real and equal");
printf ("r1=%f r2=%f",r1,r2);
} else if(d>0) {
printf ("roots are real and distinct");
printf ("r1=%f r2=%f",r1,r2);
}else {
printf ("roots are imaginary");
r1 = (-b/(2*a));
r2 = (sqrt(fabs(d)));
printf ("%f+i%f",r1,r2);
printf("%f-i%f",r1,r2);
}
} else {
printf("roots cannot be found");
}
return 0;
}
